﻿using System.Threading;
using System.Threading.Tasks;
using ExcentOne.Core.WorkerService.Model;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

// ReSharper disable once IdentifierTypo
namespace ExcentOne.Core.WorkerService
{
    public class BaseWorkerService : BackgroundService
    {
        protected internal ILogger<BaseWorkerService> Logger;

        public BaseWorkerService(ILogger<BaseWorkerService> logger, IServiceScopeFactory services)
        {
            Logger = logger;
            Services = services;
        }

        protected IServiceScopeFactory Services { get; set; }
        public IConfiguration Configuration { get; set; }

        public int Interval { get; set; }
        public string IntervalUnit { get; set; }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            Configuration = Services.CreateScope().ServiceProvider.GetRequiredService<IConfiguration>();
            return base.StartAsync(cancellationToken);
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            return Task.CompletedTask;
        }

        protected internal async Task SetWorkerInterval(CancellationToken stoppingToken)
        {
            switch (IntervalUnit)
            {
                case "minute":
                    await Task.Delay(Interval * 60 * 1000, stoppingToken);
                    break;
                case "hour":
                    await Task.Delay(Interval * 3600 * 1000, stoppingToken);
                    break;
                case "day":
                    await Task.Delay(Interval * 86400 * 1000, stoppingToken);
                    break;
                default:
                    await Task.Delay(Interval * 1000, stoppingToken);
                    break;
            }
        }
    }
}